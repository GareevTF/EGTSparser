Парсер разбирает пакет в формате протокола EGTS, 
определенный в виде массива байт в коде программы в HEX виде.

Создает из него необходимые объекты содержащие данные полей этого пакета, 
его записи, и соответствующие им подзаписи 
и выводит результат в консоль в кратком, но человеко-понятном виде.

Поля SRD подзаписей не разбираются, а только выводятся на экран.

На текущий момент реализован разбор следующих типов пакетов, сервисов и подзаписей протокола EGTS:

Тип пакета: EGTS_PT_APPDATA

Сервисы:
1) EGTS_AUTH_SERVICE
2) EGTS_TELEDATA_SERVICE

Подзаписи сервиса EGTS_AUTH_SERVICE:
1) EGTS_SR_TERM_IDENTITY
2) EGTS_SR_VEHICLE_DATA

Подзаписи сервиса EGTS_TELEDATA_SERVICE:
1) EGTS_SR_POS_DATA
2) EGTS_SR_STATE_DATA
3) EGTS_SR_AD_SENSORS_DATA


Парсер написан в Microsoft Visual Studio Community 2015 на языке C++.

Описание протокола EGTS: http://docs.cntd.ru/document/1200143253